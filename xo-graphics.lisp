(in-package :xo-graphics)

(defvar *a-var*)

(defclass test-window (gl-window)
  ((rotation :initform 0.0)))


(defmethod initialize-instance ((w test-window) &key &allow-other-keys)
  ;; It is critical you call-next-method first, or you won't get a GL
  ;; context or window.
  (call-next-method)
  ;; GL setup can go here; your GL context is automatically active,
  ;; and this is done in the main thread.
  (gl:viewport 0 0 800 600)
  (gl:matrix-mode :projection)
  (gl:ortho -2 2 -2 2 -2 2)
  (gl:matrix-mode :modelview)
  (gl:load-identity))

(defmethod render ((window test-window))
  ;; Your GL context is automatically active.  FLUSH and
  ;; SDL2:GL-SWAP-WINDOW are done implicitly by GL-WINDOW
  ;; after RENDER.
  (with-slots (rotation) window
    (gl:load-identity)
    (gl:rotate rotation 0 0 1)
    (gl:clear-color 0.0 0.0 1.0 1.0)
    (gl:clear :color-buffer)
    (gl:begin :triangles)
    (gl:color 1.0 0.0 0.0)
    (gl:vertex 0.0 1.0)
    (gl:vertex -1.0 -1.0)
    (gl:vertex 1.0 -1.0)
    (gl:end)))

(defmethod close-window ((window test-window))
  (format t "Bye!~%")
  ;; You MUST call-next-method.  But do it last, because everything
  ;; goes away when you do (your window, gl-context, etc)!
  (call-next-method))

(defmethod mousewheel-event ((window test-window) ts x y)
  (with-slots (rotation) window
    (incf rotation (* 12 y))
    (render window)))

(defvar *console-out* t)

(defmethod textinput-event ((window test-window) ts text)
  (format *console-out* "You typed: ~S~%" text)
  (when (string= "Q" (string-upcase text))
    (close-window window)))

(defmethod keyboard-event ((window test-window) state ts repeat-p keysym)
  (let ((scancode (sdl2:scancode keysym)))
    (unless repeat-p
      (format t "~A ~S~%" state scancode))
    (when (eq :scancode-escape scancode)
      (close-window window))))

(defmethod mousebutton-event ((window test-window) state ts b x y)
  (format t "~A button: ~A at ~A, ~A~%" state b x y))

(defmethod mousemotion-event ((window test-window) ts mask x y xr yr)
  (when (> mask 0)
    (format t "Mouse motion, button-mask = ~A at ~A, ~A~%" mask x y)))

;; (make-instance 'test-window)
