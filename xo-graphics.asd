(asdf:defsystem :xo-graphics
  :description "various utilities for automating shell and ssh environments"
  :long-description ""
  :version "0.0.1"
  :author ""
  :license "BSD like"
  :depends-on (:inferior-shell :osicat :cl-openstack :cl-openstack :fare-utils :sdl2 :sdl2kit)
  :components ((:file packages)
	       (:file xo-graphics :depends-on (packages))
	       (:module src
			:serial t
			:components
			((:file src)))))
